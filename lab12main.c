#include "lab12lib.h"

int menu();
void businessLogic();
void printItemMenu();
void displayBooks();
void addBook();
void sortByMark();
void sortByName();
void deleteBook();
void findBooksByMoney();

int main() {
    fileExist();
    businessLogic();
    return 0;
}

void businessLogic() {
    while(1) {
        printItemMenu();
        int choosePoint = menu();
        if(choosePoint == 1) {
            addBook();
        } else if (choosePoint == 2) {
            displayBooks();
        } else if (choosePoint == 3) {
            sortByName();
        } else if (choosePoint == 4) {
            sortByMark();
        } else if (choosePoint == 5) {
            deleteBook();
        } else if (choosePoint == 6) {
            findBooksByMoney();
        }
        else {
            printf("Exit from program\n");
            break;
        }
    }
}

int menu() {
    int choose;
    while(1){
        printf("Choose any item in menu by number:\n");
        choose = getInt();
        while(getchar() != '\n');
        if(choose > 6 || choose < 0){
            printf("Invalid item in menu. Try again!\n");
        } else {
            break;
        }
    }
    return choose;
}

void printItemMenu() {
    printf("Item menu:\n");
    printf("1 - Add book to end of file\n");
    printf("2 - Display books on the screen\n");
    printf("3 - Sort books by name\n");
    printf("4 - Sort books by mark\n");
    printf("5 - Delete book\n");
    printf("6 - Find books by money in wallet\n");
    printf("0 - Exit\n");
}

void inputRank(char *name, char *author, int *rank) {
    while(1){
        printf("Please, input rank of «%s» by «%s»:\n", name, author);
        *rank = getInt();
        while(getchar() != '\n');
        if(*rank > 10 || *rank < 0) {
            printf("Rank must be between 0 and 10\n");
        } else {
            break;
        }
    }
}

void inputCost(char* name, char* author, double *cost) {
    while(1) {
        printf("Please, input cost for «%s» by «%s»:\n", name, author);
        while (scanf("%lf", cost) != 1) {
            printf("Invalid char in int. Try again!\n");
        }
        while(getchar() != '\n');
        if(cost < 0) {
            printf("Cost must be grates than 0\n");
            printf("\n");
        } else {
            break;
        }
    }
}

void addBook() {
    fileExist();
    book new;
    printf("Please, input name of book: \n");
    inputName(new.Name);
    printf("Please, input author of «%s»:\n", new.Name);
    inputAuthor(new.Author);
    while(1) {
        printf("Please, input num of pages of «%s» by «%s»:\n", new.Name, new.Author);
        new.PageCount = getInt();
        while(getchar() != '\n');
        if(new.PageCount <= 0) {
            printf("Num of pages must be more than 0\n");
        } else {
            break;
        }
    }
    inputRank(new.Name, new.Author, &new.Rank);
    inputCost(new.Name, new.Author, &new.Cost);
    const book tmp = new;
    writeRecordEnd(&tmp, "aaa.dat");
}


void displayBooks () {
    fileExist();
    int sizeStruct = quantityStruct();
    for(int i = 0; i < sizeStruct; i++){
        book data = readRecordFromFile(i, "aaa.dat");
        printf("Name of book: %s\nAuthor: %s\nNum of pages: %d\nRank: %d\nCost: %lf\n\n", data.Name, data.Author, data.PageCount, data.Rank, data.Cost);
    }
    if(sizeStruct == 0) {
        printf("Nothing to display. Add book to file\n");
    }
}

void sortByName() {
    fileExist();
    int size = quantityStruct();
    for(int k = 0; k < size; k++) {
        for(int i = k; i < size-1; i++) {
            book r1 = readRecordFromFile(k, "aaa.dat");
            book r2 = readRecordFromFile(k+1, "aaa.dat");
            if(strcmp(r1.Name, r2.Name) > 0){
                writeRecord(k+1, &r1);
                writeRecord(k, &r2);
            }
        }
    }
    if(size == 0) {
        printf("Nothing to sort. Add book to file\n");
    }
}

void sortByMark() {
    fileExist();
    int size = quantityStruct();
    for(int k = 0; k < size; k++) {
        for(int i = k; i < size-1; i++) {
            book r1 = readRecordFromFile(k, "aaa.dat");
            book r2 = readRecordFromFile(k+1, "aaa.dat");
            if(r1.Rank > r2.Rank){
                writeRecord(k+1, &r1);
                writeRecord(k, &r2);
            }
        }
    }
    if(size == 0) {
        printf("Nothing to sort. Add book to file\n");
    }
}

void deleteBook() {
    fileExist();
    char bookName[MAX_LEN];
    int flag = 0;
    int size = quantityStruct();
    if(size == 0) {
        printf("Nothing to delete. Add book to file\n");
    } else {
        printf("Please, input name of book: \n");
        inputName(bookName);
    }
    for(int i = 0; i < size; i++) {
        book tmp = readRecordFromFile(i, "aaa.dat");
        if(strcmp(bookName, tmp.Name) == 0) {
            removeRecordFromFile(i);
            flag = 1;
        }
    }
    if(flag == 0) {
        printf("Book not found\n");
    }
}

void findBooksByMoney(){
    double wallet;
    while(1) {
        printf("Input how much money do you have:\n");
        while (scanf("%lf", &wallet) == 0) {
            printf("Invalid char in int. Try again!\n");
        }
        while(getchar() != '\n');
        if(wallet < 0) {
            printf("Wallet must be zero or positive\n");
            printf("\n");
        } else {
            break;
        }
    }
    createFile("booksForSell.dat");
    int c = 0;
    int size = quantityStruct();
    for(int i = 0; i < size; i++) {
        book tmp = readRecordFromFile(i, "aaa.dat");
        if (tmp.Cost < wallet) {
            writeRecordEnd(&tmp, "booksForSell.dat");
            c++;
        }
    }
    for(int i = 0; i < c; i++){
        book data = readRecordFromFile(i, "booksForSell.dat");
        printf("Name of book: %s\nAuthor: %s\nNum of pages: %d\nRank: %d\nCost: %lf\n\n", data.Name, data.Author, data.PageCount, data.Rank, data.Cost);
    }
}