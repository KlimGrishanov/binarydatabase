#include "lab12lib.h"

int globalVar = 10;

void createFile(char *filename) {
    FILE *file = fopen(filename, "wb");
    fclose(file);
}

int checkStr(char *string, unsigned long size){
    int c = 0;
    int ans = 1;
    for(int i = 0; i < size; i++){
        if(((int) *(string+i) < 91) && (64 < (int) *(string+i)) || ((96 < (int) *(string+i)) && ((int) *(string+i) < 123)) || ((int)*(string+i) == 32) || ((int) *(string+i)) == 39 || ((int) *(string+i)) == 45){
            c++;
        }
    }
    if(c != size){
        ans = 0;
    }
    return ans;
}

void inputAuthor(char *s){
    while(1) {
        fflush(stdin);
        fgets(s, MAX_LEN, stdin);
        fflush(stdin);
        s[strlen(s)-1]='\0';
        if (!checkStr(s, strlen(s))) {
            printf("Please dont use special symbol. Try again!\n");
        } else {
            break;
        }
    }
}

void inputName(char *s) {
    fflush(stdin);
    fgets(s, MAX_LEN, stdin);
    fflush(stdin);
    s[strlen(s)-1]='\0';
}

int getInt() {
    int x;
    while (scanf("%d", &x) == 0) {
        printf("Invalid char in int. Try again!\n");
        while(getchar() != '\n');
    }
    return x;
}

void fileExist(){
    FILE *file = fopen("aaa.dat", "rb");
    if (file != NULL) {
        fclose(file);
    } else {
        createFile("aaa.dat");
    }
}

int sizeFile() {
    FILE *file = fopen("aaa.dat", "rb");
    fseek(file, 0, SEEK_END);
    int size = ftell(file);
    fclose(file);
    return size;
}

int quantityStruct() {
    return sizeFile()/sizeof(book);
}

book readRecordFromFile(int index, char *filename) {
    FILE *file = fopen(filename, "rb");
    int size = index * sizeof(book);
    fseek(file, size, SEEK_SET);
    book data;
    fread(&data, sizeof(book), 1, file);
    fclose(file);
    return data;
}

void writeRecordEnd(const book *recordToWrite, char* filename) {
    FILE *file = fopen(filename, "ab");
    fwrite(recordToWrite, sizeof(book), 1, file);
    fflush(file);
    fclose(file);
}

void writeRecord(int index, const book *recordToWrite) {
    FILE *file = fopen("aaa.dat", "rb+");
    int offset = sizeof(book) * index;
    fseek(file, offset, SEEK_SET);
    fwrite(recordToWrite, sizeof(book), 1, file);
    fflush(file);
    fclose(file);
}

void setFileSize(int new_size){
    FILE *pfile = fopen("aaa.dat","rb+");
    int file_descriptor = fileno(pfile);
    ftruncate(file_descriptor, new_size);
    printf("%d\n", new_size);
    fclose(pfile);
}

void removeRecordFromFile(int record_index){
    FILE *pfile = fopen("aaa.dat","rb+");
    int records_count = quantityStruct();
    for (int i = record_index + 1; i < records_count; i++)
    {
        book current_record = readRecordFromFile(i, "aaa.dat");
        writeRecord(i-1, &current_record);
    }
    if(records_count > 0) {
        int new_file_size = (records_count-1) * sizeof(book);
        setFileSize(new_file_size);
    }
    fclose(pfile);
}