#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#define MAX_LEN 255

typedef struct Book {
    char Name[MAX_LEN];
    char Author[MAX_LEN];
    int PageCount;
    int Rank;
    double Cost;
} book;

// Base check
void fileExist();
void createFile(char *filename);
void removeRecordFromFile(int record_index);
void setFileSize(int new_size);
// quantity of records
int sizeFile();
int quantityStruct();

// Read and write
book readRecordFromFile(int index, char *filename);
void writeRecordEnd(const book *recordToWrite, char* filename);
void writeRecord(int index, const book *recordToWrite);

// Checks
int checkStr(char *string, unsigned long size);
void inputName(char *s);
void inputAuthor(char *s);
int getInt();